![Build Status](https://gitlab.com/pages/hexo/badges/master/build.svg)

---

# BenCodeZen

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Table of Contents*

- [Test Setup](#test-setup)
- [Getting Setup Locally](#getting-setup-locally)
- [Get in Touch](#get-in-touch)

*generated with [DocToc](https://github.com/thlorenz/doctoc)*
<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Getting Setup Locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. Install Hexo and Hexo Sever
1. Install dependencies: `npm install`
1. Preview site with: `hexo server`
1. Add content
1. Generate your site (optional): `hexo generate`

Read more at Hexo's [documentation][].

## Get in Touch

Feel free to reach out to me via the following:

[twitter]: http://www.twitter.com/bencodezen
[codepen]: http://codepen.io/BenCodeZen/
[stackoverflow]: http://stackoverflow.com/users/5100020/bencodezen
